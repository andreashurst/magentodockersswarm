FROM alankent/gsd
MAINTAINER Andreas Hurst <andreas@hurst.de>

ARG MAGENTO_REPO_USERNAME
ARG MAGENTO_REPO_PASSWORD

# Install Luma code and pre-loaded database.
ENV MAGENTO_REPO_USERNAME "admin"
ENV MAGENTO_REPO_PASSWORD "admin"
RUN sh -x /scripts/install-luma
ENV MAGENTO_REPO_USERNAME ""
ENV MAGENTO_REPO_PASSWORD ""

# Add some helper modules (optional)
ADD AndreasHurst /magento2/app/code/AndreasHurst
RUN chown -R magento:magento /magento2/app/code/AndreasHurst
RUN /usr/local/bin/mysql.server start \
 && sudo -u magento sh -c '/magento2/bin/magento setup:upgrade' \
 && rm -rf /magento2/var/* \
 && /usr/local/bin/mysql.server stop

# Add mount volume points, but often not used.
VOLUME /magento2/app/code
VOLUME /magento2/app/design
VOLUME /magento2/app/i18n
